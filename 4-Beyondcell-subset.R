#######################################
#Subsets
######################################
library("Seurat")
library("beyondcell")
library("tidyverse")
library("patchwork")
library("ggplot2")
library("dplyr")
library("ComplexHeatmap")
library("magick")
library("cowplot")

######################################  
#1)Data Import
#########################################
##### Muestra integrada de tumor por SMART-seq y 10x Anotada
#con todas las anotaciones
combitum <- readRDS("/Users/lourdesgonzalez/Desktop/TFM/Resultados/9-REMAKE_de_todo/Integracion-10x-ST-UMAP-extradata.rds")


########################################
#2) Separate cells
########################################
#forma tratando de usar 10x y smartseq
Malignas <- rownames(combitum@meta.data %>% filter(characteristics..cell.type == "Malignant"))
Malignas <- subset(combitum, cells = Malignas)
TME <- rownames(combitum@meta.data %>% filter (characteristics..cell.type %in% c("NK", "B.cell", "T.CD8", "Macrophage", "T.cell
                                                                                 ", "Mastocyte", "T.CD4", "Endothelial", "Fibroblast")))
TME <- subset(combitum, cells = TME)


########################################
# UMAPS
#######################################
Malignas <- RunUMAP(Malignas, dims = 1:10)
DimPlot(Malignas, reduction = "umap")
ggsave("Malignas_cluster_New.png")
DimPlot(Malignas, reduction = "umap", group.by = "description")
ggsave("Malignas_cluster_New.png")
DimPlot(Malignas, reduction = "umap", group.by = "characteristics..cell.type")
ggsave("Malignas_cell_type_new.png")
DimPlot(Malignas, reduction = "umap", group.by = "characteristics..sample")
ggsave("Malignas_cell_sample_new.png")

########################################
# UMAPS inmune
#######################################
TME <- RunUMAP(TME, dims = 1:10)
DimPlot(TME, reduction = "umap")
ggsave("TME_cluster10_new.png")
DimPlot(TME, reduction = "umap", group.by = "description")
ggsave("TME_description_new.png")
DimPlot(TME, reduction = "umap", group.by = "characteristics..cell.type")
ggsave("TME_celltype_new.png")
DimPlot(TME, reduction = "umap", group.by = "characteristics..sample")
ggsave("TME_sample_new.png")


#########################################
#3) FindAllMarkers 
#########################################
#### Normales ###
Malignas.markers <- FindAllMarkers(Malignas, only.pos = TRUE, min.pct = 0.25, logfc.threshold = 0.25)
Malignas.markers %>%
  group_by(cluster) %>%
  slice_max(n = 2, order_by = avg_log2FC)
write.table(Malignas.markers, file = "/Users/lourdesgonzalez/Desktop/TFM/Resultados/markers.malinas.tsv", 
            sep = "\t", row.names = TRUE, quote = FALSE, col.names = NA)


TME.markers <- FindAllMarkers(TME, only.pos = TRUE, min.pct = 0.25, logfc.threshold = 0.25)
TME.markers %>%
  group_by(cluster) %>%
  slice_max(n = 2, order_by = avg_log2FC)
write.table(TME.markers, file = "/Users/lourdesgonzalez/Desktop/TFM/Resultados/markers.TME.tsv",
            sep = "\t", row.names = TRUE, quote = FALSE, col.names = NA)

############################################  
#4) Beyondcell score with SSc Malignant
############################################
DefaultAssay(Malignas) <- "SCT"
gs <- GenerateGenesets(SSc)
nopath <- GenerateGenesets(SSc, include.pathways = FALSE)
Malignasbc <- bcScore(Malignas, gs, expr.thres = 0.1) 
saveRDS(Malignasbc, file = "/Users/lourdesgonzalez/Desktop/TFM/Resultados/Malignas-BCscore.rds")

# Run the bcUMAP function again, specifying the number of principal components you want to use.
Malignasbc <- bcUMAP(Malignasbc, k.neighbors = 20, res = 0.2)
Malignasbc <- bcUMAP(Malignasbc, pc = 25, k.neighbors = 20, res = 0.2)
bcClusters(Malignasbc, UMAP = "beyondcell", idents = "nFeature_SCT", factor.col = FALSE, pt.size = 0.1)
ggsave("nFeat_clust_prereg.png")
# Visualize whether cells are clustered based on their cell cycle status
bcClusters(Malignasbc, UMAP = "beyondcell", idents = "Phase", factor.col = TRUE, pt.size = 0.1)
ggsave("Phase_clust_prereg.png")
# Visualize whether cells are clustered based on their cell cycle stype
bcClusters(Malignasbc, UMAP = "beyondcell", idents = "characteristics..cell.type", factor.col = TRUE, pt.size = 0.1)
ggsave("ctype_clust_prereg.png")
# Visualize whether cells are clustered based on their cell sample
bcClusters(Malignasbc, UMAP = "beyondcell", idents = "characteristics..sample", factor.col = TRUE, pt.size = 0.1)
ggsave("csample_clust_prereg.png")

# Regress out sample variability
Malignasbc <- bcRegressOut(Malignasbc, vars.to.regress = "nFeature_SCT")
#Recompute UMAP
Malignasbc <- bcUMAP(Malignasbc, pc = 25, k.neighbors = 4, res = 0.2)
#bcClusters(Malignantbc, UMAP = "beyondcell", idents = "characteristics..cell.type", factor.col = TRUE, pt.size = 0.3)
#ggsave("Malignant_cluster_0.2-reg.png")
# Visualize whether cells are clustered based on the number of genes detected per each cell
bcClusters(Malignansbc, UMAP = "beyondcell", idents = "nFeature_SCT", factor.col = FALSE, pt.size = 0.1)
ggsave("Malignant_nFeat_clust_reg.png")
# Visualize whether cells are clustered based on their cell cycle status
bcClusters(Malignasbc, UMAP = "beyondcell", idents = "Phase", factor.col = TRUE, pt.size = 0.1)
ggsave("Malignant_Phase_clust_reg.png")
# Visualize whether cells are clustered based on their cell cycle stype
bcClusters(Malignasbc, UMAP = "beyondcell", idents = "characteristics..cell.type", factor.col = TRUE, pt.size = 0.1)
ggsave("Malignant_ctype_clust_reg.png")
# Visualize whether cells are clustered based on their cell sample
bcClusters(Malignasbc, UMAP = "beyondcell", idents = "characteristics..sample", factor.col = TRUE, pt.size = 0.1)
ggsave("Malignant_csample_clust_reg.png")
bcClusters(Malignasbc, UMAP = "beyondcell", idents = "bc_clusters_res.0.2", factor.col = TRUE, pt.size = 0.1)
ggsave("Malignant_csample_clust_reg.png")

####################
#Compute ranks
#####################
# Obtain general statistics
Malignasbc <- bcRanks(Malignasbc)
# Obtain unextended therapeutic cluster-based statistics
Malignasbc <- bcRanks(Malignasbc, idents = "characteristics..cell.type", extended = FALSE)
Malignasbc <- bcRanks(Malignasbc, idents = "characteristics..sample", extended = FALSE)


#######################################
# Visualize drug signatures and markers Malignas
#######################################
##### Sample #####
unique(Malignasbc@meta.data$characteristics..sample)
write.table(Malignasbc@ranks$characteristics..sample, col.names = NA, sep = "\t", file = "/Users/lourdesgonzalez/Desktop/TFM/Resultados/bc-mal-rank-cellsample-T.txt")
unique(Malignasbc@meta.data$characteristics..sample)
bc4Squares(Malignasbc, idents = "characteristics..sample", lvl = "SyS7", top = 5)
ggsave("Malignas-chasample-SyS7.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(Malignasbc, idents = "characteristics..sample", lvl = "SyS5", top = 5)
ggsave("Malignas-chasample-SyS5.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(Malignasbc, idents = "characteristics..sample", lvl = "SyS2", top = 5)
ggsave("Malignas-chasample-SyS2.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(Malignasbc, idents = "characteristics..sample", lvl = "SyS1", top = 5)
ggsave("Malignas-chasample-SyS1.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(Malignasbc, idents = "characteristics..sample", lvl = "SyS11.met", top = 5)
ggsave("Malignas-chasample-SyS11.met.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(Malignasbc, idents = "characteristics..sample", lvl = "SyS11", top = 5)
ggsave("Malignas-chasample-SyS11.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(Malignasbc, idents = "characteristics..sample", lvl = "SyS10", top = 5)
ggsave("Malignas-chasample-SyS10.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(Malignasbc, idents = "characteristics..sample", lvl = "SyS12", top = 5)
ggsave("Malignas-chasample-SyS12.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(Malignasbc, idents = "characteristics..sample", lvl = "SyS12pt", top = 5)
ggsave("Malignas-chasample-SyS12pt.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(Malignasbc, idents = "characteristics..sample", lvl = "SyS13", top = 5)
ggsave("Malignas-chasample-SyS13.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(Malignasbc, idents = "characteristics..sample", lvl = "SyS14", top = 5)
ggsave("Malignas-chasample-SyS14.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(Malignasbc, idents = "characteristics..sample", lvl = "SyS16", top = 5)
ggsave("Malignas-chasample-SyS16.png", width = 30, height = 20, units = "in", scale = 0.5)



############################################  
#Beyondcell score con SSc TME
############################################
DefaultAssay(TME) <- "SCT"
TMEbc <- bcScore(TME, gs, expr.thres = 0.1) 
# Run the bcUMAP function again, specifying the number of principal components you want to use.
TMEbc <- bcUMAP(TMEbc, k.neighbors = 20, res = 0.2)
TMEbc <- bcUMAP(TMEbc, pc = 25, k.neighbors = 20, res = 0.2)
bcClusters(TMEbc, UMAP = "beyondcell", idents = "nFeature_SCT", factor.col = FALSE, pt.size = 0.1)
ggsave("nFeat_clust_prereg-TME.png")
# Visualize whether cells are clustered based on their cell cycle status
bcClusters(TMEbc, UMAP = "beyondcell", idents = "Phase", factor.col = TRUE, pt.size = 0.1)
ggsave("Phase_clust_prereg-TME.png")
# Visualize whether cells are clustered based on their cell cycle stype
bcClusters(TMEbc, UMAP = "beyondcell", idents = "characteristics..cell.type", factor.col = TRUE, pt.size = 0.1)
ggsave("ctype_clust_prereg-TME.png")
# Visualize whether cells are clustered based on their cell sample
bcClusters(TMEbc, UMAP = "beyondcell", idents = "characteristics..sample", factor.col = TRUE, pt.size = 0.1)
ggsave("csample_clust_prereg-TME.png")

# Regress out sample variability
TMEbc <- bcRegressOut(TMEbc, vars.to.regress = "nFeature_SCT")
#Recompute UMAP
TMEbc <- bcUMAP(TMEbc, pc = 25, k.neighbors = 4, res = 0.2)
bcClusters(TMEbc, UMAP = "beyondcell", idents = "characteristics..cell.type", factor.col = TRUE, pt.size = 0.3)
ggsave("Celltype-TME.png")
# Visualize whether cells are clustered based on the number of genes detected per each cell
bcClusters(TMEbc, UMAP = "beyondcell", idents = "nFeature_SCT", factor.col = FALSE, pt.size = 0.1)
ggsave("nFeat_clust_reg-TME.png")
# Visualize whether cells are clustered based on their cell cycle status
bcClusters(TMEbc, UMAP = "beyondcell", idents = "Phase", factor.col = TRUE, pt.size = 0.1)
ggsave("Phase_clust_reg-TME.png")
# Visualize whether cells are clustered based on their cell cycle stype
bcClusters(TMEbc, UMAP = "beyondcell", idents = "characteristics..cell.type", factor.col = TRUE, pt.size = 0.1)
ggsave("ctype_clust_reg-TME.png")
# Visualize whether cells are clustered based on their cell sample
bcClusters(TMEbc, UMAP = "beyondcell", idents = "characteristics..sample", factor.col = TRUE, pt.size = 0.1)
ggsave("csample_clust_reg-TME.png")
bcClusters(TMEbc, UMAP = "beyondcell", idents = "bc_clusters_res.0.2", factor.col = TRUE, pt.size = 0.1)
ggsave("bc_cluster_reg-TME.png")


###########################################################################################################
# Visualize drug signatures and markers TME
###########################################################################################################

####################
#Compute ranks
#####################
# Obtain general statistics
TMEbc <- bcRanks(TMEbc)
# Obtain unextended therapeutic cluster-based statistics
TMEbc <- bcRanks(TMEbc, idents = "characteristics..cell.type", extended = FALSE)
TMEbc <- bcRanks(TMEbc, idents = "characteristics..sample", extended = FALSE)
unique(TMEbc@meta.data$characteristics..sample)
bc4Squares(TMEbc, idents = "characteristics..sample ", lvl = "SyS7", top = 5)
ggsave("TMEbc-chasample-SyS7.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(TMEbc, idents = "characteristics..sample", lvl = "SyS5", top = 5)
ggsave("TMEbc-chasample-SyS5.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(TMEbc, idents = "characteristics..sample", lvl = "SyS2", top = 5)
ggsave("TMEbc-chasample-SyS2.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(TMEbc, idents = "characteristics..sample", lvl = "SyS1", top = 5)
ggsave("TMEbc-chasample-SyS1.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(TMEbc, idents = "characteristics..sample", lvl = "SyS11.met", top = 5)
ggsave("TMEbc-chasample-SyS11.met.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(TMEbc, idents = "characteristics..sample", lvl = "SyS11", top = 5)
ggsave("TMEbc-chasample-SyS11.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(TMEbc, idents = "characteristics..sample", lvl = "SyS10", top = 5)
ggsave("TMEbc-chasample-SyS10.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(TMEbc, idents = "characteristics..sample", lvl = "SyS12", top = 5)
ggsave("TMEbc-chasample-SyS12.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(TMEbc, idents = "characteristics..sample", lvl = "SyS12pt", top = 5)
ggsave("TMEbc-chasample-SyS12pt.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(TMEbc, idents = "characteristics..sample", lvl = "SyS13", top = 5)
ggsave("TMEbc-chasample-SyS13.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(TMEbc, idents = "characteristics..sample", lvl = "SyS14", top = 5)
ggsave("TMEbc-chasample-SyS14.png", width = 30, height = 20, units = "in", scale = 0.5)
bc4Squares(TMEbc, idents = "characteristics..sample", lvl = "SyS16", top = 5)
ggsave("TMEbc-chasample-SyS16.png", width = 30, height = 20, units = "in", scale = 0.5)

#########################################
# Unir bcscores a archivos originales
#########################################
#Crear columna en objeto Malignas que sean los cluster de Malignasbc
Malignas@meta.data$bc_clusters_res.0.2 <- Malignas@meta.data$NAME
NewTME <- TME@meta.data[which(TME@meta.data$NAME %in% TMEbc@meta.data$bc_clusters_res.0.2),]
Malignas@meta.data[which(Malignas@meta.data$bc_clusters_res.0.2 %in% Malignasbc@meta.data$bc_clusters_res.0.2)]
Malignas@meta.data$bc_clusters_res.0.2 <- Malignasbc@meta.data$bc_clusters_res.0.2[which(Malignasbc@meta.data$NAME %in% Malignas@meta.data$bc_clusters_res.0.2)]
#Crear columna en objeto TME que sean los cluster de TMEbc 
TME@meta.data$bc_clusters_res.0.2 <- TME@meta.data$NAME
TME@meta.data[which(TME@meta.data$bc_clusters_res.0.2 %in% TMEbc@meta.data$bc_clusters_res.0.2)]
TME@meta.data$bc_clusters_res.0.2 <- TMEbc@meta.data$bc_clusters_res.0.2[which(TMEbc@meta.data$NAME %in% TME@meta.data$bc_clusters_res.0.2)]


#########################################
#5) FindAllMarkers para clustersterapeuticos
#########################################
#### Malignas ###
Idents(Malignas) <- "bc_clusters_res.0.2"
Malignasbc.marker <- FindAllMarkers(Malignas, only.pos = TRUE, min.pct = 0.25, logfc.threshold = 0.25)
Malignasbc.marker %>%
  group_by(cluster) %>%
  slice_max(n = 2, order_by = avg_log2FC)
write.table(Malignasbc.marker, file = "/Users/lourdesgonzalez/Desktop/TFM/Resultados/markers.malinasbc.teraclust.tsv", 
            sep = "\t", row.names = TRUE, quote = FALSE, col.names = NA)

#### TME ###
Idents(TME) <- "bc_clusters_res.0.2"
TME.markers.teraclusters <- FindAllMarkers(TME, only.pos = TRUE, min.pct = 0.25, logfc.threshold = 0.25)
TME.markers.teraclusters %>%
  group_by(cluster) %>%
  slice_max(n = 2, order_by = avg_log2FC)
write.table(TME.markers.teraclusters, file = "/Users/lourdesgonzalez/Desktop/TFM/Resultados/markers.TME.teraclust.tsv",
            sep = "\t", row.names = TRUE, quote = FALSE, col.names = NA)